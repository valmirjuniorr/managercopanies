package backend

import grails.gorm.transactions.Transactional

@Transactional
class DataService {
    CompanyService companyService
    StockService stockService
    CompanyStocksService companyStocksService

    def populate() {
        createCompanies()

        createStocks()
    }

    def createCompanies() {
        companyService.save("Amazon", "Everything")
        companyService.save("Apple" , "Technology")
        companyService.save("Ifood" , "Food")
    }

    def createStocks() {
        Calendar now, startDay, endDay
        
        now = Calendar.getInstance()
        now.set(Calendar.MINUTE, 0)
        now.set(Calendar.SECOND, 0)
        now.set(Calendar.MILLISECOND, 0)

        Company.list().each{ company ->
           for(int i = -30; i<=0; i++ ){
               startDay = now.clone()
               startDay.add(Calendar.DAY_OF_MONTH, i)
               
               endDay = startDay.clone()

               startDay.set(Calendar.HOUR_OF_DAY, 10)
               endDay.set(Calendar.HOUR_OF_DAY, 18)

               while( startDay.compareTo(endDay) <= 0 && startDay.compareTo(now) <= 0){
                   stockService.save(randomPrice(), startDay.getTime(), company )
                   
                   startDay.add(Calendar.MINUTE, 1)
               }
           }
        }
    }

    def randomPrice() {
        Random random = new Random()

        random.nextDouble() + random.nextInt(100) + 1
    }


}
