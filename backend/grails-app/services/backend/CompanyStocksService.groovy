package backend

import grails.gorm.transactions.Transactional

@Transactional
class CompanyStocksService {

    def getStocks(Company company, Integer numbersOfHoursUntilNow) {
        Calendar calendarStart = Calendar.getInstance()
        Calendar calendar = calendarStart.clone()
        
        calendar.add(Calendar.HOUR_OF_DAY, -numbersOfHoursUntilNow)

        def stocks = Stock.afterDate(calendar.getTime()).findAllByCompany(company)

        stocks.each{
            println(it)
        }

        def timeSpent = Calendar.getInstance().getTimeInMillis() - calendarStart.getTimeInMillis()

        println "The time spent was: $timeSpent milisseconds"
        println "It was  was retrieved  ${stocks.size()}"
    }
}
