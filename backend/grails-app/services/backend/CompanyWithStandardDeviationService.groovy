package backend

import grails.gorm.transactions.Transactional
import groovy.sql.Sql
import groovy.sql.GroovyRowResult

@Transactional
class CompanyWithStandardDeviationService {

    def dataSource

    List<GroovyRowResult> index() {

        String query = '''\
                SELECT c.* ,
                       SQRT( SUM( (s.price - s_ag.AVG_PRICE) * (s.price - s_ag.AVG_PRICE) ) / s_ag.COUNT_STOCKS ) 
                          as standard_deviation 
                FROM  COMPANY  as c
                INNER JOIN STOCK as s
                INNER JOIN (
                    SELECT company_id, COUNT(id) as COUNT_STOCKS,  avg(price) as AVG_PRICE FROM STOCK 
                    GROUP BY company_id
                ) as s_ag  on c.id = s_ag.company_id 
                group by c.id  
        '''

        Sql sql = new Sql(dataSource)

        sql.rows(query).collect{[
            id:  it.id,
            name:  it.name,
            segment:  it.segment,
            standardDeviation:  it.standard_deviation,    
        ]}
    }     

}
