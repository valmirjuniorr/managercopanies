package backend

import grails.gorm.services.Service

@Service(Stock)
interface StockService {
    Stock save(Double price, Date priceDate, Company company)
}
