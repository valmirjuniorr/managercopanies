package backend

class BootStrap {

    DataService dataService
    def init = { servletContext ->
        dataService.populate()
    }
    
    def destroy = {
    }
}
