package backend

class CompanyStocksController {

    def index(Integer companyId) { 
        render(contentType: "application/json") {
            stocks(Company.get(companyId).stocks){
                id it.id
                price it.price
                priceDate it.priceDate
            }
        }
    }
}
