package backend

class CompanyWithStandardDeviationController {
    
    CompanyWithStandardDeviationService companyWithStandardDeviationService

    def index() {
        respond companyWithStandardDeviationService.index(), formats:['json']
    }
}
