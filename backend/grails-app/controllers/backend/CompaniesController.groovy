package backend

class CompaniesController {

    def index() { 
        render(contentType: "application/json") {
            companies(Company.list()){
                id it.id
                name it.name
                segment it.segment
            }
        }
    }
}
