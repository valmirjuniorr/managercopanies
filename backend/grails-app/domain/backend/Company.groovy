package backend

class Company {
    String name
    String segment

    static hasMany = [stocks: Stock]

    static constraints = {
        name size: 3..28, blank: false, unique: true
        segment size: 3..255, blank: false        
    }

    String toString(){
        name
    }
}
