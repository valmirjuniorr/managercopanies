package backend

class Stock {

    Double price
    Date priceDate
    Company company

    static belongsTo = [company: Company]

    static constraints = {
        price min: 0D, scale:2, blank: false
        priceDate blank: false
    }

    static namedQueries = {
       afterDate { date ->
           gt 'priceDate', date
       }
   }

    String toString(){
        "$price || $priceDate"
    }
}
