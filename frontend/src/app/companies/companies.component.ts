import { Component, OnInit } from '@angular/core';
import { CompanyService } from '../company.service';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.scss']
})
export class CompaniesComponent implements OnInit {
  companies = []

  constructor(private companyService: CompanyService) { }

  ngOnInit() {}

  getCompanies(): void {
    this.companyService.getCompanies().subscribe(companies => this.companies = companies);
  }
}
