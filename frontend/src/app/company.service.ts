import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Company } from './company';

@Injectable({
  providedIn: 'root'
})

export class CompanyService {
  private companiesUrl = 'http://localhost:8080/companyWithStandardDeviation'

  constructor( private http: HttpClient) { }

  getCompanies(): Observable<Company[]> {
    return this.http.get<Company[]>(this.companiesUrl) .pipe(
      catchError( (error: HttpErrorResponse) => [] )
    )
  }
}
