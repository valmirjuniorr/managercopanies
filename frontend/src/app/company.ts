export interface Company {
  id: number;
  name: string;
  segment: string;
  standardDeviation: number;
}


